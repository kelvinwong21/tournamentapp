package ca.shibatek.tournament.service;

import ca.shibatek.tournament.authentication.CognitoIdentityProvider;
import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.CognitoIdentityProviderException;
import software.amazon.awssdk.services.cognitoidentityprovider.model.SignUpRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.SignUpResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UsernameExistsException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class CognitoAuthenticationServiceTest {

    @Mock
    private CognitoIdentityProvider cognitoIdentityProvider;
    @Mock
    private CognitoIdentityProviderClient cognitoIdentityProviderClient;
    @Mock
    private SignUpResponse signUpResponse;
    @Mock
    private SdkHttpResponse sdkHttpResponse;
    private CognitoAuthenticationService fixture;
    private UserDto userDto;

    @BeforeEach
    public void setUp(){
        this.fixture = new CognitoAuthenticationService(cognitoIdentityProvider);
        this.userDto = new UserDto();
        userDto.setFirstName("Miho");
        userDto.setLastName("Tsuno");
        userDto.setEmail("miho_tsuno@java.net");
        userDto.setPassword("password");
    }

    @ParameterizedTest
    @ValueSource(classes = {UsernameExistsException.class, CognitoIdentityProviderException.class})
    @DisplayName("Should throw RegistrationException when CognitoIdentityProviderException is thrown")
    public void shouldThrowRegistrationExceptionWhenAWSExceptionIsThrown(final Class exception) {
        mockCognitoIdentityProviderMethods();
        when(cognitoIdentityProviderClient.signUp(any(SignUpRequest.class))).thenThrow(exception);

        assertThrows(RegistrationException.class, () -> fixture.registerUser(userDto));
    }

    @Test
    @DisplayName("Should return Optional with User when signup succeeds")
    public void shouldReturnOptionalWithUserWhenSignupSucceeds() throws Exception{
        mockCognitoIdentityProviderMethods();
        when(cognitoIdentityProviderClient.signUp(any(SignUpRequest.class))).thenReturn(signUpResponse);
        when(signUpResponse.sdkHttpResponse()).thenReturn(sdkHttpResponse);
        when(sdkHttpResponse.isSuccessful()).thenReturn(true);

        User mockSignup = fixture.registerUser(userDto);
        assertNotNull(mockSignup);
    }

    @Test
    @DisplayName("Should throw RegistrationException when SDK isSuccessful is false")
    public void shouldThrowRegistrationExceptionWhenSDKIsSuccessfulIsFalse(){
        mockCognitoIdentityProviderMethods();
        when(cognitoIdentityProviderClient.signUp(any(SignUpRequest.class))).thenReturn(signUpResponse);
        when(signUpResponse.sdkHttpResponse()).thenReturn(sdkHttpResponse);
        when(sdkHttpResponse.isSuccessful()).thenReturn(false);

        when(sdkHttpResponse.statusText()).thenReturn(Optional.of("Test"));
        when(sdkHttpResponse.headers()).thenReturn(null);

        assertThrows(RegistrationException.class, () -> fixture.registerUser(userDto));
    }

    private void mockCognitoIdentityProviderMethods(){
        when(cognitoIdentityProvider.getCognitoIdentityProviderClient()).thenReturn(cognitoIdentityProviderClient);
        when(cognitoIdentityProvider.getAwsClientId()).thenReturn("clientId");
        when(cognitoIdentityProvider.getAwsClientSecret()).thenReturn("secret");
    }
}
