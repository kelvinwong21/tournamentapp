package ca.shibatek.tournament.util;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Base64EncoderUtilTest {

    private static final String TEST_STRING = "SORA_SHINAA";

    @Test
    public void shouldReturnBase64EncodedValueForCognitoWhenPassingString(){
        final String actual = Base64EncoderUtil.calculateCognitoHash("Test", "Test",
                TEST_STRING);

        assertTrue(Base64.isBase64(actual));
    }

}
