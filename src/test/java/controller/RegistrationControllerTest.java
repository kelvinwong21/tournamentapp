package controller;

import ca.shibatek.tournament.controller.RegistrationController;
import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.model.User;
import ca.shibatek.tournament.service.RegistrationException;
import ca.shibatek.tournament.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.ResultMatcher.matchAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {

    private static final String USER_EMAIL_KEY = "userEmail";
    private static final String USER_DTO_KEY = "userDto";
    private static final String REGISTRATION_ERROR_KEY = "registrationError";

    @Mock
    private UserService userService;
    private RegistrationController fixture;
    private MockMvc mockMvc;
    private User user;


    @BeforeEach
    public void setup(){
        fixture = new RegistrationController(userService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(fixture).build();
        this.user = new User();
        user.setFirstName("Erika");
        user.setLastName("Kitagawa");
        user.setEmail("erika_kitagawa1@yahoo.com");
    }

    @DisplayName("Should return signup view when /register page requested and contain model for userDto")
    @Test
    public void shouldReturnSignUpViewNameWhenPageRequested() throws Exception{
        mockMvc.perform(get("/register")).andDo(print())
                .andExpect(
                        matchAll(
                            status().isOk(),
                            MockMvcResultMatchers.view().name("signup"),
                            model().size(1),
                            model().attributeExists("userDto"))
                );
    }

    @DisplayName("Should not display confirmation page if user email attribute does not exist")
    @Test
    public void shouldNotDisplayConfirmationPageIfEmailAttributeDoesNotExist () throws Exception{
        mockMvc.perform(get("/confirmation")).andDo(print())
                .andExpect(
                        matchAll(
                                status().is3xxRedirection(),
                                redirectedUrl("/register")
                        )
                );
    }

    @DisplayName("Should redirect to confirmation page with one flash attribute after sign up")
    @Test
    public void shouldRedirectToConfirmationPageAfterSignUp() throws Exception{
        when(userService.registerUser(any(UserDto.class))).thenReturn(user);

        MultiValueMap<String, String> postParams = getPostParams();
        postParams.add("password", "P@ssword!2");
        postParams.add("passwordConfirmation", "P@ssword!2");

        mockMvc.perform(post("/register")
                .params(postParams)).andDo(print())
                .andExpect(
                        matchAll(
                                status().is3xxRedirection(),
                                redirectedUrl("/confirmation"),
                                flash().attributeCount(1),
                                flash().attributeExists(USER_EMAIL_KEY)
                        )
                );
    }

    @DisplayName("Should have errors when form validation fails")
    @Test
    public void shouldHaveFlashAttributesForErrorMessageWhenSignUpFails() throws Exception{
        MultiValueMap<String, String> postParams = getPostParams();
        postParams.add("password", "123");
        postParams.add("passwordConfirmation", "456");

        mockMvc.perform(post("/register")
                .params(postParams)).andDo(print())
                .andExpect(
                        matchAll(
                                model().hasErrors(),
                                model().attributeExists(USER_DTO_KEY)
                        )
                );
    }

    @DisplayName("Should catch RegistrationException when userService fails")
    @Test
    public void shouldCatchRegistrationExceptionWhenUserServiceFails() throws Exception{
        doThrow(new RegistrationException("Mock exception"))
                .when(userService)
                .registerUser(any(UserDto.class));

        MultiValueMap<String, String> postParams = getPostParams();
        postParams.add("password", "P@ssword!2");
        postParams.add("passwordConfirmation", "P@ssword!2");

        mockMvc.perform(post("/register")
                .params(postParams)).andDo(print())
                .andExpect(
                        matchAll(
                            model().attributeExists(REGISTRATION_ERROR_KEY)
                        )
                );
    }

    private MultiValueMap getPostParams(){
        MultiValueMap<String, String> postParams = new LinkedMultiValueMap<>();

        postParams.add("firstName", user.getFirstName());
        postParams.add("lastName", user.getLastName());
        postParams.add("email", user.getEmail());

        return postParams;
    }
}
