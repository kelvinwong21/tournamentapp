package ca.shibatek.tournament.validation;

import ca.shibatek.tournament.constraint.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

    private Matcher matcher;
    public static final Pattern EMAIL_PATTERN =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public void initialize(ValidEmail constraintAnnotation){}

    @Override
    public boolean isValid(final String email, ConstraintValidatorContext context){
        return (validateEmail(email));
    }

    private boolean validateEmail(final String email){
        matcher = EMAIL_PATTERN.matcher(email);

        return matcher.matches();
    }
}
