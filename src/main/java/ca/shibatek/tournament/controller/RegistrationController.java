package ca.shibatek.tournament.controller;

import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.model.User;
import ca.shibatek.tournament.service.RegistrationException;
import ca.shibatek.tournament.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@Log4j2
public class RegistrationController {

    private static final String USER_EMAIL_KEY = "userEmail";
    private static final String USER_DTO_KEY = "userDto";
    private static final String REGISTRATION_ERROR_KEY = "registrationError";
    private final UserService userService;

    @Autowired
    public RegistrationController(final UserService userService){
        this.userService = userService;
    }

    @GetMapping("/register")
    public String signUpForm(Model model){
        if(!model.containsAttribute(USER_DTO_KEY)){
            model.addAttribute(USER_DTO_KEY, new UserDto());
        }
        return "signup";
    }

    @GetMapping("/confirmation")
    public String registered(Model model){
        if(model.asMap().get(USER_EMAIL_KEY) == null){
            return "redirect:/register";
        }

        return "confirmation";
    }

    @PostMapping("/register")
    public String signUpCreateUser(@Valid UserDto userDto, final BindingResult bindingResult,
                                   Model model, RedirectAttributes redirectAttributes){

        User newUser = null;
        if(bindingResult.hasErrors()){
            model.addAttribute(userDto);
            log.warn("User sign-up form had errors; userDto=" + userDto.toString());
            return "signup";
        }

        try{
            newUser = userService.registerUser(userDto);
        } catch(RegistrationException ex){
            model.addAttribute(REGISTRATION_ERROR_KEY, ex.getMessage());
            log.warn("User sign-up failed; reason="+ ex.getMessage() + ";userDto=" + userDto.toString());
            log.warn(ex);
            return "signup";
        }

        log.info("User registration succeeded; user=" + userDto.toString());
        redirectAttributes.addFlashAttribute(USER_EMAIL_KEY, newUser.getEmail());
        return "redirect:/confirmation";
    }

}
