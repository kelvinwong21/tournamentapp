package ca.shibatek.tournament.service;

import ca.shibatek.tournament.authentication.CognitoIdentityProvider;
import ca.shibatek.tournament.dto.UserDto;
import ca.shibatek.tournament.model.User;
import ca.shibatek.tournament.util.Base64EncoderUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.*;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class CognitoAuthenticationService implements UserService {

    private static final String EMAIL_ATTRIBUTE = "email";
    private static final String FIRST_NAME_ATTRIBUTE = "given_name";
    public static final String FAMILY_NAME_ATTRIBUTE = "family_name";

    private CognitoIdentityProvider cognitoIdentityProvider;

    @Autowired
    public CognitoAuthenticationService(CognitoIdentityProvider cognitoIdentityProvider){
        this.cognitoIdentityProvider = cognitoIdentityProvider;
    }

    @Override
    public User registerUser(UserDto userDto) throws RegistrationException{
        User user = null;
        CognitoIdentityProviderClient cognitoClient = cognitoIdentityProvider.getCognitoIdentityProviderClient();
        List<AttributeType> attributeTypesList = getSignUpRequestFromDto(userDto);

        SignUpRequest signUpRequest = buildSignUpRequest(userDto, attributeTypesList);
        try{
            SignUpResponse signUpResponse = cognitoClient.signUp(signUpRequest);
            SdkHttpResponse sdkHttpResponse = signUpResponse.sdkHttpResponse();

            if(!sdkHttpResponse.isSuccessful()){
                readFailedSignUpResponse(sdkHttpResponse);
            } else{
                User registeredUser = new User();
                registeredUser.setId(signUpResponse.userSub());
                registeredUser.setEmail(userDto.getEmail());
                registeredUser.setFirstName(userDto.getFirstName());
                registeredUser.setLastName(userDto.getLastName());
                registeredUser.setOrganizer(userDto.isOrganizer());
                user = registeredUser;
            }
        } catch (UsernameExistsException ex){
            log.error("Username exists; email=" + userDto.getEmail());
            throw new RegistrationException("Email is invalid or already exists");
        } catch(CognitoIdentityProviderException ex){
            log.error("CognitoIdentityProviderException",ex);
            throw new RegistrationException("Internal Server Error");
        }
        return user;
    }


    private List<AttributeType> getSignUpRequestFromDto(final UserDto userDto){
        List<AttributeType> listOfAttributeTypes = new ArrayList<>();

        AttributeType emailAttribute =  AttributeType.builder().name(EMAIL_ATTRIBUTE).value(userDto.getEmail()).build();
        AttributeType firstNameAttribute = AttributeType.builder().name(FIRST_NAME_ATTRIBUTE)
                .value(userDto.getFirstName()).build();
        AttributeType lastNameAttribute = AttributeType.builder().name(FAMILY_NAME_ATTRIBUTE)
                .value(userDto.getLastName()).build();

        listOfAttributeTypes.add(emailAttribute);
        listOfAttributeTypes.add(firstNameAttribute);
        listOfAttributeTypes.add(lastNameAttribute);

        return listOfAttributeTypes;
    }

    private SignUpRequest buildSignUpRequest(final UserDto userDto, List<AttributeType> attributeTypesList){
        final String secretHash = Base64EncoderUtil.calculateCognitoHash(cognitoIdentityProvider.getAwsClientId(),
                cognitoIdentityProvider.getAwsClientSecret(), userDto.getEmail());

        SignUpRequest signUpRequest = SignUpRequest.builder()
                .clientId(cognitoIdentityProvider.getAwsClientId())
                .secretHash(secretHash)
                .username(userDto.getEmail())
                .password(userDto.getPassword())
                .userAttributes(attributeTypesList)
                .build();

        return signUpRequest;
    }

    private void readFailedSignUpResponse(final SdkHttpResponse sdkHttpResponse) throws RegistrationException{
        final String errorMessage = "Sign up failed; Status text="
                + sdkHttpResponse.statusText().get()
                + "Headers: "
                + sdkHttpResponse.headers();
        log.error(errorMessage);
        throw new RegistrationException(sdkHttpResponse.statusText().get());
    }
}
